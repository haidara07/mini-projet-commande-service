const asyncErrorHandler = require('../middlewares/asyncErrorHandler');
const Order = require('../models/orderModel');
const ErrorHandler = require('../utils/errorHandler');

// Create New Order
exports.newOrder = asyncErrorHandler(async (req, res, next) => {

    const {
        totalPrice,
        quantity,
        productId,
        userId
    } = req.body;


    const order = await Order.create({
        quantity,
        totalPrice,
        userId,
        productId
    });

    res.status(201).json({
        success: true,
        order,
    });
});

// Get Single Order Details
exports.getSingleOrderDetails = asyncErrorHandler(async (req, res, next) => {

    const order = await Order.findById(req.params.id)

    if (!order) {
        return next(new ErrorHandler("Order Not Found", 404));
    }

    res.status(200).json({
        success: true,
        order,
    });
});

// Update Order Status 
exports.updateOrder = asyncErrorHandler(async (req, res, next) => {

    const order = await Order.findById(req.params.id);

    if (!order) {
        return next(new ErrorHandler("Order Not Found", 404));
    }

    order.orderStatus = "payée";

    await order.save({ validateBeforeSave: false });

    res.status(200).json({
        success: true
    });
});

