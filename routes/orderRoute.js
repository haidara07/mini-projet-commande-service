const express = require('express');
const { newOrder, getSingleOrderDetails, updateOrder } = require('../controllers/orderController');

const router = express.Router();

router.route('/order/new').post(newOrder);
router.route('/order/:id').get( getSingleOrderDetails);
router.route('/order/:id').put(updateOrder)

module.exports = router;